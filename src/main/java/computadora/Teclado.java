
package computadora;


public class Teclado extends DispositivoEntrada {
   private int idTeclado;
   private int contadorTeclado;
    public Teclado(String tipoEntrada, String marca) {
        super(tipoEntrada, marca);
    }

    @Override
    public String toString() {
        return "Teclado{" + "idTeclado=" + idTeclado + ", contadorTeclado=" + contadorTeclado + '}';
    }
    
}
