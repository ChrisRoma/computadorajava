
package computadora;

public class Orden {
   private final int idOrden;
   private final Computadora[] computadoras;
   private int contadorOrdenes;
   private static int contadorComputadoras;
   private static final int MAXCOMPUTADORAS=15;
   
   private Orden(){
   this.idOrden=++contadorComputadoras;
   this.computadoras= new Computadora[Orden.MAXCOMPUTADORAS];
   
   }
   /**/
   public void agregarComputadora(Computadora computadora){
       if(this.contadorOrdenes<Orden.MAXCOMPUTADORAS){
       computadoras[contadorOrdenes++]=computadora;/*contadorOrdenes arranca en cero y recien suma cuando termina este metodo*/
     }else{
           System.out.print("no se pueden agregar mas computadoras porque "
                   + "se ha superado el numero");
       }}
   
 
    public void mostrarOrden(){
   for (int i=0;i<this.contadorOrdenes;i++){
      System.out.println(this.computadoras[i]);
    }
   }
}
